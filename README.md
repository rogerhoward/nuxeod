# nuxeod
A starting point for a local Nuxeo dev environment using Docker Compose. Clone this project and run `docker-compose up` to spin up an environment.

## Nuxeo
There are three volume bind mounts to make local dev easier by exposing Nuxeo container directories as local host directories.

### config
The local config file at `./_config/nuxeo/custom.conf` is mounted into the Nuxeo container at `/docker-entrypoint-initnuxeo.d/nuxeo.conf`, which gets appended to the default Nuxeo config on startup. This makes it easy to edit `custom.conf` locally and see the changes in Nuxeu on restart.

### data
The Nuxeo data directory is mounted locally at `./_data/nuxeo/` for easy debugging.

### logs
The Nuxeo log directory is mounted locally at `./_logs/nuxeo/` for easy debugging.

## MySQL
While MySQL shouldn't need much customization, there are a few tweaks I include with the custom image that have always made things easier.

1. `--default-authentication-plugin=mysql_native_password` is set on startup to support legacy passwords, which many applications require.
2. If custom MySQL configs are needed, they can be added to `./mysql/config/nuxeo.cnf` which is loaded on startup by MySQL after all other MySQL config files.
3. Any .sql files added to `./mysql/sqlinit` will be executed against the MySQL database on first startup. This is useful for setting up schema and adding custom data on first run.

## Other notes and warnings

1. Passwords and other secrets should be set in a .env file, not in the `docker-compose.yml`
2. `NUXEO_PACKAGES` doesn't work as I assumed - it logs an error `org.nuxeo.connect.update.PackageException: Package not found: "nuxeo-web-ui"`. It appears may need to register with Nuxeo online first before installing packages, but not sure how this should work in a deployment.
3.
